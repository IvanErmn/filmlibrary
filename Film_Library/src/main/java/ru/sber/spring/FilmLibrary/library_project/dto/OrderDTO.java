package ru.sber.spring.FilmLibrary.library_project.dto;

import jakarta.persistence.Column;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.*;
import ru.sber.spring.FilmLibrary.library_project.model.Film;
import ru.sber.spring.FilmLibrary.library_project.model.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderDTO extends GenericDTO {
    private FilmDTO filmDTO;

    private LocalDateTime rentDate;
    private Long rentPeriod;
    private Boolean purchase;

    private Long filmId;
    private Long userId;
}
