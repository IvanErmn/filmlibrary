package ru.sber.spring.FilmLibrary.library_project.annotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MySecuredAnnotation {
    String[] value();
}

