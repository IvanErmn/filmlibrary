package ru.sber.spring.FilmLibrary.library_project.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Утильный класс для работы с датами/строками.
 */
public class DateFormatter {
    private DateFormatter() {
    }

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd"); //Задаем желаемый формат даты.

    /***
     * Метод форматирующий поданную на вход строку в нужный формат даты и возвращающий дату.
     * @param dateToFormat - Строка, которую нужно преобразовать в дату.
     * @return - LocalDate: дата в нужном формате.
     */
    public static LocalDate formatStringToDate(final String dateToFormat) {
        return LocalDate.parse(dateToFormat, FORMATTER);
    }
}
