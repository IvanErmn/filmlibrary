package ru.sber.spring.FilmLibrary.dbexample.constants;

public interface DBConstants {
//    jdbc:postgresql://localhost:5432/local_film_db
    String DB_HOST = "localhost";
    String DB = "local_film_db";
    String USER = "postgres";
    String PASSWORD = "12345";
    String PORT = "5432";
}
