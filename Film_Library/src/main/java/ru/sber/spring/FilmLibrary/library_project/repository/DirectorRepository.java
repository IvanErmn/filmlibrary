package ru.sber.spring.FilmLibrary.library_project.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.sber.spring.FilmLibrary.library_project.model.Director;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {
    Page<Director> findAllByIsDeletedFalse(Pageable pageable);

    Page<Director> findAllByDirectorFioContainsIgnoreCaseAndIsDeletedFalse(String fio, Pageable pageable);

    @Query(value = """
          select case when count(a) > 0 then false else true end
          from Director a join a.films b
                        join Order bri on b.id = bri.film.id
          where a.id = :directorId
          and bri.purchase = true
          """)
    boolean checkDirectorForDeletion(final Long directorId);
}
