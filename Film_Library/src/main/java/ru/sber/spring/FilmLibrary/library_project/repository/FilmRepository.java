package ru.sber.spring.FilmLibrary.library_project.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sber.spring.FilmLibrary.library_project.model.Film;

@Repository
public interface FilmRepository extends GenericRepository<Film> {
    @Query(nativeQuery = true,
            value = """
                 select distinct b.*
                 from films b
                 left join film_directors ba on b.id = ba.film_id
                 join directors a on a.id = ba.director_id
                 where b.title ilike '%' || btrim(coalesce(:title, b.title)) || '%'
                 and cast(b.genre as char) like coalesce(:genre,'%')
                 and a.directors_fio ilike '%' || :directors_fio || '%'
                 and b.is_deleted = false
                      """)
    Page<Film> searchFilms(@Param(value = "genre") String genre,
                           @Param(value = "title") String title,
                           @Param(value = "directors_fio") String fio,
                           Pageable pageable);

    Film findFilmByIdAndOrdersPurchaseTrueAndIsDeletedFalse(final Long id);

    @Query("""
          select case when count(b) > 0 then false else true end
          from Film b join Order bri on b.id = bri.film.id
          where b.id = :id and bri.purchase = true
          """)
    boolean checkFilmForDeletion(final Long id);

    Page<Film> findAllByIsDeletedFalse(Pageable pageable);
}
