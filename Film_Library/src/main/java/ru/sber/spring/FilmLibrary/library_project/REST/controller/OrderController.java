package ru.sber.spring.FilmLibrary.library_project.REST.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import ru.sber.spring.FilmLibrary.library_project.dto.OrderDTO;
import ru.sber.spring.FilmLibrary.library_project.model.Order;
import ru.sber.spring.FilmLibrary.library_project.service.OrderService;

@RestController
@RequestMapping("/orders")
@Tag(name = "Покупки", description = "Контроллер для работы с покупками Фильмов")
public class OrderController extends GenericController<Order, OrderDTO> {
    public OrderController(OrderService orderService) {
        super(orderService);
    }

//    private final OrderRepository orderRepository;
//    private final UserRepository userRepository;
//    private final FilmRepository filmRepository;
//    public OrderController(OrderRepository orderRepository, UserRepository userRepository, FilmRepository filmRepository) {
//        super(orderRepository);
//        this.userRepository = userRepository;
//        this.filmRepository = filmRepository;
//        this.orderRepository = orderRepository;
//    }

//    @Operation(description = "Добавить Фильм и Пользователя к заказу", method = "addFilm")
//    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Order> addOrder(@RequestParam(value = "filmId") Long filmId,
//                                          @RequestParam(value = "orderId") Long orderId,
//                                          @RequestParam(value = "userId") Long userId) {
//        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм с переданным ID не найден"));
//        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("Пользователь с таким ID не найден"));
//        Order order = orderRepository.findById(orderId).orElseThrow(() -> new NotFoundException("Заказ с таким ID не найден"));
//        user.getOrders().add(order);
//        film.getOrders().add(order);
//        return ResponseEntity.status(HttpStatus.OK).body(orderRepository.save(order));
//    }
}