package ru.sber.spring.FilmLibrary.library_project.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.sber.spring.FilmLibrary.library_project.dto.GenericDTO;
import ru.sber.spring.FilmLibrary.library_project.exception.MyDeleteException;
import ru.sber.spring.FilmLibrary.library_project.mapper.GenericMapper;
import ru.sber.spring.FilmLibrary.library_project.model.GenericModel;
import ru.sber.spring.FilmLibrary.library_project.repository.GenericRepository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Абстрактный сервис, который хранит в себе реализацию CRUD операций по-умолчанию.
 * Если реализация отлична от того, что представлено в этом классе,
 * то она переопределяется в реализации конкретного сервиса.
 *
 * @param <T> - Сущность, с которой мы работаем.
 * @param <N> - DTO, которую мы будем отдавать/принимать дальше.
 */
@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {
    protected final GenericRepository<T> genericRepository; //Инжектим абстрактный репозиторий для работы с базой данных
    protected final GenericMapper<T, N> genericMapper; //Инжектим абстрактный маппер для преобразований из DTO -> Entity, и обратно

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository, GenericMapper<T, N> genericMapper) {
        this.genericRepository = repository;
        this.genericMapper = genericMapper;
    }



// СПИСКИ -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public List<N> listAll() {
        return genericMapper.toDTOs(genericRepository.findAll());
    }
    public Page<N> listAll(Pageable pageable) {
        Page<T> objects = genericRepository.findAll(pageable);
        List<N> result = genericMapper.toDTOs(objects.getContent());
        return new PageImpl<>(result, pageable, objects.getTotalElements());
    }
    public Page<N> listAllNotDeleted(Pageable pageable) {
        Page<T> preResult = genericRepository.findAllByIsDeletedFalse(pageable);
        List<N> result = genericMapper.toDTOs(preResult.getContent());
        return new PageImpl<>(result, pageable, preResult.getTotalElements());
    }
    public List<N> listAllNotDeleted() {
        return genericMapper.toDTOs(genericRepository.findAllByIsDeletedFalse());
    }
    /***
     * Получить информацию о конкретном объекте/сущности по ID.
     * @param id - идентификатор сущности для поиска.
     * @return - конкретная сущность в формате DTO
     */
    public N getOne(final Long id) {
        return genericMapper.toDTO(genericRepository.findById(id).orElseThrow(() -> new NotFoundException("Данных по заданному ID " + id + " не найдено")));
    }



// СОЗДАТЬ, ОБНОВИТЬ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    /***
     * Создание сущности в БД.
     * @param newObject - информация о сущности/объекте.
     * @return - сохраненная в БД сущность в формате DTO.
     */
    public N create(N newObject) {
        newObject.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        newObject.setCreatedWhen(LocalDateTime.now());
        return genericMapper.toDTO(genericRepository.save(genericMapper.toEntity(newObject)));
    }

    /***
     * Обновление сущности в БД.
     * @param updatedObject - информация о сущности/объекте.
     * @return - обновленная в БД сущность в формате DTO.
     */
    public N update(N updatedObject) {
        return genericMapper.toDTO(genericRepository.save(genericMapper.toEntity(updatedObject)));
    }



// УДАЛЕНИЕ ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    /***
     * Софт Удаление сущности (пометка на удаление).*
     * @param id - идентификатор сущности, которая должна быть помечена на удаление.
     */
    public void deleteSoft(Long id) throws MyDeleteException {
        T obj = genericRepository.findById(id).orElseThrow(() -> new NotFoundException("Объект не найден с айди: " + id));
        markAsDeleted(obj);
        genericRepository.save(obj);
    }

    /**
     * Восстановление помеченной записи, как удаленной.
     * @param id - идентификатор сущности, которая должна быть восстановлена
     */
    public void restore(Long id) {
        T obj = genericRepository.findById(id).orElseThrow(() -> new NotFoundException("Объект не найден с айди: " + id));
        unMarkAsDeleted(obj);
        genericRepository.save(obj);
    }

    /***
     * Хард Удаление сущности из БД.
     * @param id - идентификатор сущности, которая должна быть удалена.
     */
    public void deleteHard(Long id) throws MyDeleteException {
        genericRepository.deleteById(id);
    }



// ПОМЕТКА: УДАЛЕНО --------------------------------------------------------------------------------------------------------------------------------------------------------------
    public void markAsDeleted(GenericModel genericModel) {
        genericModel.setDeleted(true);
        genericModel.setDeletedWhen(LocalDateTime.now());
        genericModel.setDeletedBy(SecurityContextHolder.getContext().getAuthentication().getName());
    }
    public void unMarkAsDeleted(GenericModel genericModel) {
        genericModel.setDeleted(false);
        genericModel.setDeletedWhen(null);
        genericModel.setDeletedBy(null);
    }
}
