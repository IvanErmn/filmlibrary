package ru.sber.spring.FilmLibrary.library_project.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.sber.spring.FilmLibrary.library_project.dto.FilmDTO;
import ru.sber.spring.FilmLibrary.library_project.dto.OrderDTO;
import ru.sber.spring.FilmLibrary.library_project.mapper.OrderMapper;
import ru.sber.spring.FilmLibrary.library_project.model.Order;
import ru.sber.spring.FilmLibrary.library_project.repository.OrderRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {
    private final FilmService filmService;
    private final OrderMapper orderMapper;
    private OrderRepository orderRepository;

    protected OrderService(OrderRepository orderRepository,
                           OrderMapper orderMapper,
                           FilmService filmService) {
        super(orderRepository, orderMapper);
        this.filmService = filmService;
        this.orderMapper = orderMapper;
        this.orderRepository = orderRepository;
    }

    public Page<OrderDTO> listOrders(final Long id,
                                                   final Pageable pageable) {
        Page<Order> objects = orderRepository.getOrderByUserId(id, pageable);
        List<OrderDTO> results = orderMapper.toDTOs(objects.getContent());
        return new PageImpl<>(results, pageable, objects.getTotalElements());
    }

    public OrderDTO rentFilm(OrderDTO orderDTO) {
        FilmDTO filmDTO = filmService.getOne(orderDTO.getFilmId());
        filmService.update(filmDTO);
        long rentPeriod = orderDTO.getRentPeriod() != null ? orderDTO.getRentPeriod() : 14L;
        orderDTO.setRentDate(LocalDateTime.now());
        orderDTO.setPurchase(true);
        orderDTO.setRentPeriod((long) rentPeriod);
        orderDTO.setCreatedWhen(LocalDateTime.now());
        orderDTO.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        return orderMapper.toDTO(genericRepository.save(genericMapper.toEntity(orderDTO)));
    }

    public void returnFilm(final Long id) {
        OrderDTO orderDTO = getOne(id);
        orderDTO.setPurchase(false);
        FilmDTO filmDTO = orderDTO.getFilmDTO();
        update(orderDTO);
        filmService.update(filmDTO);
    }












}
