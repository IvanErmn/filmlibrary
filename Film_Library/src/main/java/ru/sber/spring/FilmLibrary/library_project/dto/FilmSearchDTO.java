package ru.sber.spring.FilmLibrary.library_project.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.sber.spring.FilmLibrary.library_project.model.Genre;

@Getter
@Setter
@ToString
public class FilmSearchDTO {
    private String title;
    private String directorFio;
    private Genre genre;
}