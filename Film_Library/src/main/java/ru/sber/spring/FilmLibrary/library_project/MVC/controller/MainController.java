package ru.sber.spring.FilmLibrary.library_project.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Hidden
public class MainController {
    @GetMapping("/") // - если обращение на Главную страницу, то отображается index.html:
    public String index() {
        return "index";
    }
}
