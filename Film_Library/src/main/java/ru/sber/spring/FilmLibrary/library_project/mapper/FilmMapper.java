package ru.sber.spring.FilmLibrary.library_project.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.spring.FilmLibrary.library_project.dto.FilmDTO;
import ru.sber.spring.FilmLibrary.library_project.model.Film;
import ru.sber.spring.FilmLibrary.library_project.model.GenericModel;
import ru.sber.spring.FilmLibrary.library_project.repository.DirectorRepository;
import ru.sber.spring.FilmLibrary.library_project.repository.FilmRepository;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Film, FilmDTO> {
    private final DirectorRepository directorRepository;
    protected FilmMapper(ModelMapper modelMapper, DirectorRepository directorRepository) {
        super(modelMapper, Film.class, FilmDTO.class);
        this.directorRepository = directorRepository;
    }

    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setDirectorsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmDTO source, Film destination) {
        if(!Objects.isNull(source.getDirectorsIds())){
            destination.setDirectors(new HashSet<>(directorRepository.findAllById(source.getDirectorsIds())));
        } else {
            destination.setDirectors(null);
        }
    }
    @Override
    protected void mapSpecificFields(Film source, FilmDTO destination) {
        destination.setDirectorsIds((getIds(source)));
    }

    @Override
    protected Set<Long> getIds(Film film) {
        return Objects.isNull(film) || Objects.isNull(film.getDirectors())
                ? null
                : film.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
