package ru.sber.spring.FilmLibrary.library_project.dto;

import jakarta.persistence.Column;
import jakarta.persistence.OneToMany;
import lombok.*;
import ru.sber.spring.FilmLibrary.library_project.model.Order;
import ru.sber.spring.FilmLibrary.library_project.model.Role;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserDTO extends GenericDTO {
    private String login;
    private String password;

// FIO:
    private String firstName;
    private String lastName;
    private String middleName;

// Other:
    private String birthDate;
    private String phone;
    private String address;
    private String email;
    private String changePasswordToken;
    private boolean isDeleted;

    private RoleDTO role;
    private Set<Long> orderIds;
}
