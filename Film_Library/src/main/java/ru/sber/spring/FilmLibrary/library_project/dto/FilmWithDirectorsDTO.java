package ru.sber.spring.FilmLibrary.library_project.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.spring.FilmLibrary.library_project.model.Film;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmWithDirectorsDTO extends FilmDTO {
    private Set<DirectorDTO> directors;
    public FilmWithDirectorsDTO(Film film, Set<DirectorDTO> directors) {
        super(film);
        this.directors = directors;
    }
}
