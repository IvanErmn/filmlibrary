package ru.sber.spring.FilmLibrary.library_project.REST.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.FilmLibrary.library_project.dto.DirectorDTO;
import ru.sber.spring.FilmLibrary.library_project.model.Director;
import ru.sber.spring.FilmLibrary.library_project.service.DirectorService;

@RestController
@RequestMapping("/directors") //localhost:9090/api/rest/directors
@Tag(name = "Директоры", description = "Контроллер для работы с Директорами фильмов Фильмотеки")
public class DirectorController extends GenericController<Director, DirectorDTO> {
    public DirectorController(DirectorService directorService) {
        super(directorService);
    }

//    @Operation(description = "Добавить фильм к директору", method = "addFilm")
//    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Director> addDirector(@RequestParam(value = "filmId") Long filmId,
//                                          @RequestParam(value = "directorId") Long directorId) {
//        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм с переданным ID не найден"));
//        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Директор с таким ID не найден"));
//        director.getFilms().add(film);
//        return ResponseEntity.status(HttpStatus.OK).body(directorRepository.save(director));
//    }
}
