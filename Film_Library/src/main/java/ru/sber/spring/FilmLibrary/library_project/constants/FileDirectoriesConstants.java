package ru.sber.spring.FilmLibrary.library_project.constants;

public interface FileDirectoriesConstants {
    String FILMS_UPLOAD_DIRECTORY = "files/films";
}
