package ru.sber.spring.FilmLibrary.library_project.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import ru.sber.spring.FilmLibrary.library_project.dto.OrderDTO;
import ru.sber.spring.FilmLibrary.library_project.model.Order;
import ru.sber.spring.FilmLibrary.library_project.repository.FilmRepository;
import ru.sber.spring.FilmLibrary.library_project.repository.UserRepository;
import ru.sber.spring.FilmLibrary.library_project.service.FilmService;

import java.util.Set;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDTO> {
    private final UserRepository userRepository;
    private final FilmRepository filmRepository;
    private final FilmService filmService;
    protected OrderMapper(ModelMapper modelMapper,
                          UserRepository userRepository,
                          FilmRepository filmRepository,
                          FilmService filmService) {
        super(modelMapper, Order.class, OrderDTO.class);
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
        this.filmService = filmService;
    }

    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(Order.class, OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setFilmId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(OrderDTO::setUserId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(OrderDTO::setFilmDTO)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(OrderDTO.class, Order.class)
                .addMappings(m -> m.skip(Order::setFilm)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter());
    }

// как связывать (маппить) остальные поля:
    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow(() -> new NotFoundException("Фильм не найден")));
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователь не найден")));
    }
    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setUserId(source.getUser().getId());
        destination.setFilmId(source.getFilm().getId());
        destination.setFilmDTO(filmService.getOne(source.getFilm().getId()));
    }

// не используется:
    @Override
    protected Set<Long> getIds(Order entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}
