package ru.sber.spring.FilmLibrary.library_project.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;
import ru.sber.spring.FilmLibrary.library_project.constants.Errors;
import ru.sber.spring.FilmLibrary.library_project.dto.FilmDTO;
import ru.sber.spring.FilmLibrary.library_project.dto.FilmSearchDTO;
import ru.sber.spring.FilmLibrary.library_project.dto.FilmWithDirectorsDTO;
import ru.sber.spring.FilmLibrary.library_project.exception.MyDeleteException;
import ru.sber.spring.FilmLibrary.library_project.mapper.FilmMapper;
import ru.sber.spring.FilmLibrary.library_project.mapper.FilmWithDirectorsMapper;
import ru.sber.spring.FilmLibrary.library_project.model.Film;
import ru.sber.spring.FilmLibrary.library_project.repository.FilmRepository;
import ru.sber.spring.FilmLibrary.library_project.utils.FileHelper;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class FilmService extends GenericService<Film, FilmDTO> {
    private final FilmRepository filmRepository;
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;
    protected FilmService(FilmRepository filmRepository, FilmMapper filmMapper, FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(filmRepository, filmMapper);
        this.filmRepository = filmRepository;
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
    }



// СПИСКИ ФИЛЬМОВ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    public Page<FilmWithDirectorsDTO> getAllFilmsWithDirectors(Pageable pageable) {
        Page<Film> filmsPaginated = genericRepository.findAll(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }
    public Page<FilmWithDirectorsDTO> getAllNotDeletedFilmsWithDirectors(Pageable pageable) {
        Page<Film> filmsPaginated = genericRepository.findAllByIsDeletedFalse(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }
    public FilmWithDirectorsDTO getFilmWithDirectors(Long id) {
        return filmWithDirectorsMapper.toDTO(genericMapper.toEntity(super.getOne(id)));
    }
    public Page<FilmWithDirectorsDTO> findFilms(FilmSearchDTO filmSearchDTO, Pageable pageable) {
        String genre = filmSearchDTO.getGenre() != null ? String.valueOf(filmSearchDTO.getGenre().ordinal()) : null;
        Page<Film> filmsPaginated = filmRepository.searchFilms(genre,
                filmSearchDTO.getTitle(),
                filmSearchDTO.getDirectorFio(),
                pageable
        );
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }



// СОЗДАТЬ И ОБНОВИТЬ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    public FilmDTO create(final FilmDTO object, MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        return genericMapper.toDTO(filmRepository.save(genericMapper.toEntity(object)));
    }
    public FilmDTO update(final FilmDTO object, MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        return genericMapper.toDTO(genericRepository.save(genericMapper.toEntity(object)));
    }



// УДАЛЕНИЕ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    @Override
    public void deleteSoft(Long id) throws MyDeleteException {
        Film film = genericRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Книги с заданным ID=" + id + " не существует"));
        boolean filmCanBeDeleted = filmRepository.checkFilmForDeletion(id);
        if (filmCanBeDeleted) {
            if (film.getOnlineCopyPath() != null && !film.getOnlineCopyPath().isEmpty()) {
                FileHelper.deleteFile(film.getOnlineCopyPath());
            }
            markAsDeleted(film);
            genericRepository.save(film);
        }
        else {
            throw new MyDeleteException(Errors.Films.FILM_DELETE_ERROR);
        }
    }
    public void restore(Long objectId) {
        Film film = genericRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Книги с заданным ID=" + objectId + " не существует"));
        unMarkAsDeleted(film);
        genericRepository.save(film);
    }
}
