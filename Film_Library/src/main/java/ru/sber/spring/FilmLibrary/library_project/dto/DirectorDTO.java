package ru.sber.spring.FilmLibrary.library_project.dto;

import jakarta.persistence.Column;
import lombok.*;
import ru.sber.spring.FilmLibrary.library_project.model.Director;
import ru.sber.spring.FilmLibrary.library_project.model.Film;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DirectorDTO extends GenericDTO {
    private String directorFio;
    private String description;
    private String title;
    private Set<Long> filmsIds;
    private Set<Film> films;
    private boolean isDeleted;

    public DirectorDTO(Director director) {
        DirectorDTO directorDTO = new DirectorDTO();
        //из entity делаем DTO
        directorDTO.setTitle(director.getTitle());
        directorDTO.setDirectorFio(director.getDirectorFio());
        directorDTO.setId(director.getId());
        directorDTO.setDeleted((director.isDeleted()));
        directorDTO.setDescription(director.getDescription());
        Set<Film> films = director.getFilms();
        Set<Long> filmsIds = new HashSet<>();
        if (films != null && films.size() > 0) {
            films.forEach(a -> filmsIds.add(a.getId()));
        }
        directorDTO.setFilmsIds(filmsIds);
        this.films = films;
    }
}
