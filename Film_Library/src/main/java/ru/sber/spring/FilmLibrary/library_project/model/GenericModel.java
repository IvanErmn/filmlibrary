package ru.sber.spring.FilmLibrary.library_project.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serial;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
@ToString
public class GenericModel {
    @Serial
    private static final long serialVersionUID = 1;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_get")
    private Long id;
    @Column(name = "created_when", nullable = false)
    private LocalDateTime createdWhen;
    @Column(name = "created_by", nullable = false)
    private String createdBy;

    @Column(name = "is_deleted", columnDefinition = "boolean default false")
    private boolean isDeleted;
    @Column(name = "deleted_when")
    private LocalDateTime deletedWhen;
    @Column(name = "deleted_by")
    private String deletedBy;
}
