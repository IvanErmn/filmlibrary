// [JAVA13][ДЗ№6] - задача на проверку от Еремина Ивана Романовича

package ru.sber.spring.FilmLibrary.dbexample.dao;

import org.springframework.stereotype.Component;
import ru.sber.spring.FilmLibrary.dbexample.model.Book;
import ru.sber.spring.FilmLibrary.dbexample.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class UserDaoBean {
    private final Connection connection;

    public UserDaoBean(Connection connection) {
        this.connection = connection;
    }

    public void addUser(User user) throws SQLException { // добавление 1 клиента БД
        if (isUser(user)) {
            System.out.println("\nКлиент с номером " + user.getPhoneNumber() + " уже есть!");
        } else {
            PreparedStatement sqlQuery = connection.prepareStatement("insert into clients values (?,?,?,?,?)");
            sqlQuery.setString(1, user.getSurname());
            sqlQuery.setString(2, user.getName());
            sqlQuery.setString(3, user.getBirthDay());
            sqlQuery.setString(4, user.getPhoneNumber());
            sqlQuery.setString(5, user.getEMail());
            sqlQuery.execute();
            System.out.println("\n+1 клиент добавлен в базу");
        }

        if (user.getBooks() != null) {
            for (int i = 0; i < user.getBooks().size(); i++) {
                if (isBookTaken((user.getBooks().get(i)), user)) {
                    user.getBooks().set(i, null);
                } else {
                    if (!isBookTakenByUser(user.getBooks().get(i), user)) {
                        PreparedStatement sqlQuery2 = connection.prepareStatement("insert into booksTaken values (?,?)");
                        sqlQuery2.setString(1, user.getPhoneNumber());
                        sqlQuery2.setInt(2, user.getBooks().get(i).getBookId());
                        sqlQuery2.execute();
                    }
                }
            }
            user.getBooks().removeAll(Collections.singleton(null));
        }
    }


// Добавлен ли пользователь в БД: Clients (да/нет):
    public boolean isUser(User user) throws SQLException {
        PreparedStatement sqlQuery = connection.prepareStatement("select Телефон from clients where Телефон = ?");
        sqlQuery.setString(1, user.getPhoneNumber());

        ResultSet resultSet = sqlQuery.executeQuery();
        return (resultSet.next());
    }


// есть ли книга у пользователя (да/нет):
    public boolean isBookTakenByUser(Book book, User user) throws SQLException {
        PreparedStatement sqlQuery = connection.prepareStatement("select id_book from booksTaken\n" +
                "where id_book = ? and Телефон = ?");
        sqlQuery.setInt(1, book.getBookId());
        sqlQuery.setString(2, user.getPhoneNumber());
        ResultSet resultSet = sqlQuery.executeQuery();
        return (resultSet.next());
    }


// Занята ли книга другими пользователями (да/нет):
    public boolean isBookTaken(Book book, User user) throws SQLException {
        PreparedStatement sqlQuery = connection.prepareStatement("select id_book from booksTaken\n" +
                "where id_book = ? and Телефон != ?");
        sqlQuery.setInt(1, book.getBookId());
        sqlQuery.setString(2, user.getPhoneNumber());
        ResultSet resultSet = sqlQuery.executeQuery();
        return (resultSet.next());
    }


// Напечатать все книги у пользователя:
    public void printBooksByUser(String phoneNumber) throws SQLException {
        PreparedStatement sqlQuery = connection.prepareStatement("select k.\"Фамилия\", k.\"Имя\", k.\"Телефон\", t.\"id_book\", b.title, b.author\n" +
                "from clients k join booksTaken t on k.\"Телефон\" = t.\"Телефон\" join books b on t.id_book = b.\"id\"\n" +
                "where k.\"Телефон\" = ?");
        sqlQuery.setString(1, phoneNumber);
        ResultSet resultSet = sqlQuery.executeQuery();

        // Получаем и выводим в консоль результат:
        System.out.println("\nСписок книг клиента с номером " + phoneNumber + ":");
        if (resultSet.next()) {
            System.out.println(" - " +
                    resultSet.getString("Фамилия") + " " +
                    resultSet.getString("Имя") + ", " +
                    resultSet.getString("Телефон") + ", id книги: " +
                    resultSet.getString("id_book") + ", " +
                    resultSet.getString("author") + " \"" +
                    resultSet.getString("title") + "\""
            );
            while (resultSet.next()) {
                System.out.println(" - " +
                        resultSet.getString("Фамилия") + " " +
                        resultSet.getString("Имя") + ", " +
                        resultSet.getString("Телефон") + ", id книги: " +
                        resultSet.getString("id_book") + ", " +
                        resultSet.getString("author") + " \"" +
                        resultSet.getString("title") + "\""
                );
            }
        } else System.out.println(" - ...");
    }


// Список книг пользователя:
    public List<String> listBooksOfUser(String phoneNumber) throws SQLException {
                PreparedStatement sqlQuery = connection.prepareStatement("select b.title " +
                        "from clients k join booksTaken t on k.\"Телефон\" = t.\"Телефон\" join books b on t.id_book = b.\"id\"" +
                        "where k.\"Телефон\" = ?");
        sqlQuery.setString(1, phoneNumber);
        ResultSet resultSet = sqlQuery.executeQuery();

        List<String> books = new ArrayList<>();
        while (resultSet.next()) {
            books.add(resultSet.getString("title"));
        }
        return books;
    }
}
