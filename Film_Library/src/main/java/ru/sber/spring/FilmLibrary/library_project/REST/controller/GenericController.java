package ru.sber.spring.FilmLibrary.library_project.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import ru.sber.spring.FilmLibrary.library_project.dto.GenericDTO;
import ru.sber.spring.FilmLibrary.library_project.exception.MyDeleteException;
import ru.sber.spring.FilmLibrary.library_project.model.GenericModel;
import ru.sber.spring.FilmLibrary.library_project.repository.GenericRepository;
import ru.sber.spring.FilmLibrary.library_project.service.GenericService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public abstract class GenericController<T extends GenericModel, N extends GenericDTO> {
    private final GenericService<T, N> genericService;
    public GenericController(GenericService<T, N> genericService) {
        this.genericService = genericService;
    }

    @Operation(description = "Получить запись по ID", method = "getById")
    @RequestMapping(value = "/getById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> getById(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(genericService.getOne(id));
    }

    @Operation(description = "Получить все записи", method = "getAll")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<N>> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(genericService.listAll());
    }

    @Operation(description = "Создать новую запись", method = "create")
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> create(@RequestBody N newEntity) {
        newEntity.setCreatedWhen(LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.CREATED).body(genericService.create(newEntity));
    }

    @Operation(description = "Обновить запись", method = "update")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> update(@RequestBody N updatedEntity, @RequestParam(value = "id") Long id) {
        updatedEntity.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED).body(genericService.update(updatedEntity));
    }

    @Operation(description = "Удалить запись по ID", method = "delete")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteSoft(@PathVariable(value = "id") Long id) throws MyDeleteException {
        genericService.deleteSoft(id);
    }

    @Operation(description = "Удалить запись по ID", method = "delete")
    @RequestMapping(value = "/delete/hard/{id}", method = RequestMethod.DELETE)
    public void deleteHard(@PathVariable(value = "id") Long id) throws MyDeleteException {
        genericService.deleteHard(id);
    }
}

