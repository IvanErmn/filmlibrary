package ru.sber.spring.FilmLibrary.library_project.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "films")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "films_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class Film extends GenericModel {
    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "premier_year", nullable = false)
    private Integer premierYear;
    @Column(name = "country", nullable = false)
    private String country;
    @Column(name = "film_company")
    private String filmCompany;
    @Column(name = "description", nullable = false, length = 1000)
    private String description;
    @Column(name = "online_copy_path")
    private String onlineCopyPath;
    @Column(name = "genre", nullable = false)
    @Enumerated
    private Genre genre;

    @ManyToMany
    @JoinTable(name = "film_directors",
            joinColumns = @JoinColumn(name = "film_id"), foreignKey = @ForeignKey(name = "FK_FILM_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name = "director_id"), inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILM"))
    private Set<Director> directors; // связь с классом Director

    @OneToMany(mappedBy = "film")
    private Set<Order> orders;

}
