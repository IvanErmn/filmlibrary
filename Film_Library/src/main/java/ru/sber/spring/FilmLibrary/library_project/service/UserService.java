package ru.sber.spring.FilmLibrary.library_project.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.sber.spring.FilmLibrary.library_project.constants.MailConstants;
import ru.sber.spring.FilmLibrary.library_project.dto.RoleDTO;
import ru.sber.spring.FilmLibrary.library_project.dto.UserDTO;
import ru.sber.spring.FilmLibrary.library_project.mapper.UserMapper;
import ru.sber.spring.FilmLibrary.library_project.model.User;
import ru.sber.spring.FilmLibrary.library_project.repository.UserRepository;
import ru.sber.spring.FilmLibrary.library_project.utils.MailUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static ru.sber.spring.FilmLibrary.library_project.constants.UserRoleConstants.ADMIN;

@Service
public class UserService extends GenericService<User, UserDTO> {
    private final JavaMailSender javaMailSender;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    protected UserService(UserRepository userRepository,
                          UserMapper userMapper,
                          BCryptPasswordEncoder bCryptPasswordEncoder,
                          JavaMailSender javaMailSender) {
        super(userRepository, userMapper);
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.javaMailSender = javaMailSender;
    }



// ПОЛУЧИТЬ ПОЛЬЗОВАТЕЛЯ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    public UserDTO getUserByLogin(final String login) {
        return genericMapper.toDTO(((UserRepository) genericRepository).findUserByLogin(login));
    }
    public UserDTO getUserByEmail(final String email) {
        return genericMapper.toDTO(((UserRepository) genericRepository).findUserByEmail(email));
    }
    public Page<UserDTO> findUsers(UserDTO userDTO, Pageable pageable) {
        Page<User> users = ((UserRepository) genericRepository).searchUsers(userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getLogin(),
                pageable);
        List<UserDTO> result = genericMapper.toDTOs(users.getContent());
        return new PageImpl<>(result, pageable, users.getTotalElements());
    }



// СОЗДАТЬ ПОЛЬЗОВАТЕЛЯ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if (ADMIN.equalsIgnoreCase(userName)) {
            roleDTO.setId(2L); //участник
        }
        else {
            roleDTO.setId(1L); //пользователь
        }
        object.setRole(roleDTO);
        object.setCreatedBy("REGISTRATION FORM");
        object.setCreatedWhen(LocalDateTime.now());
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        return genericMapper.toDTO(genericRepository.save(genericMapper.toEntity(object)));
    }



// ПАРОЛИ ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public Boolean checkPassword(String password, UserDetails userDetails) {
        return bCryptPasswordEncoder.matches(password, userDetails.getPassword());
    }
    public void sendChangePasswordEmail(final UserDTO userDTO) {
        UUID uuid = UUID.randomUUID();
        userDTO.setChangePasswordToken(uuid.toString());
        update(userDTO);
        SimpleMailMessage mailMessage = MailUtils.createEmailMessage(userDTO.getEmail(),
                MailConstants.MAIL_SUBJECT_FOR_REMEMBER_PASSWORD,
                MailConstants.MAIL_MESSAGE_FOR_REMEMBER_PASSWORD + uuid);
        javaMailSender.send(mailMessage);
    }
    public void changePassword(final String uuid, final String password) {
        UserDTO user = genericMapper.toDTO(((UserRepository) genericRepository).findUserByChangePasswordToken(uuid));
        user.setChangePasswordToken(null);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        update(user);
    }
    public List<String> getUserEmailsWithDelayedRentDate() {
        return ((UserRepository) genericRepository).getDelayedEmails();
    }
}
