package ru.sber.spring.FilmLibrary.library_project.exception;

public class MyDeleteException extends Exception {
    public MyDeleteException(String message) {
        super(message);
    }
}
