package ru.sber.spring.FilmLibrary.library_project.REST.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.FilmLibrary.library_project.dto.FilmDTO;
import ru.sber.spring.FilmLibrary.library_project.model.Film;
import ru.sber.spring.FilmLibrary.library_project.service.FilmService;

@RestController
@RequestMapping("/films")
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмами Фильмотеки")
public class FilmController extends GenericController<Film, FilmDTO> {
    public FilmController(FilmService filmService) {
        super(filmService);
    }

//    private final FilmRepository filmRepository;
//    private final DirectorRepository directorRepository;
//    public FilmController(FilmRepository filmRepository, DirectorRepository directorRepository) {
//        super(filmRepository);
//        this.filmRepository = filmRepository;
//        this.directorRepository = directorRepository;
//    }

//    @Operation(description = "Добавить директора к фильму", method = "addDirector")
//    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Film> addDirector(@RequestParam(value = "filmId") Long filmId,
//                                          @RequestParam(value = "directorId") Long directorId) {
//        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм с переданным ID не найден"));
//        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Директор с таким ID не найден"));
//        film.getDirectors().add(director);
//        return ResponseEntity.status(HttpStatus.OK).body(filmRepository.save(film));
//    }
}
