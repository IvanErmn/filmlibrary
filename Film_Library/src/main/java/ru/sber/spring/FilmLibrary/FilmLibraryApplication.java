package ru.sber.spring.FilmLibrary;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.sql.SQLException;

@SpringBootApplication
@EnableScheduling
public class FilmLibraryApplication implements CommandLineRunner /*(интерфейс для запуска web приложения*/ {
	public static void main(String[] args) {
		SpringApplication.run(FilmLibraryApplication.class, args);
	}

	@Override
	public void run(String... args) throws SQLException {	}
}
