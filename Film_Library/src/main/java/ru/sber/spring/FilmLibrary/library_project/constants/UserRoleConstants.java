package ru.sber.spring.FilmLibrary.library_project.constants;

public interface UserRoleConstants {
    String ADMIN = "ADMIN";
    String DIRECTOR = "DIRECTOR";
    String USER = "USER";
}
