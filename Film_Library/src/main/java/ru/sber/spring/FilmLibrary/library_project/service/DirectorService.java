package ru.sber.spring.FilmLibrary.library_project.service;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.sber.spring.FilmLibrary.library_project.constants.Errors;
import ru.sber.spring.FilmLibrary.library_project.dto.AddFilmDTO;
import ru.sber.spring.FilmLibrary.library_project.dto.DirectorDTO;
import ru.sber.spring.FilmLibrary.library_project.dto.FilmDTO;
import ru.sber.spring.FilmLibrary.library_project.dto.FilmWithDirectorsDTO;
import ru.sber.spring.FilmLibrary.library_project.exception.MyDeleteException;
import ru.sber.spring.FilmLibrary.library_project.mapper.DirectorMapper;
import ru.sber.spring.FilmLibrary.library_project.mapper.FilmMapper;
import ru.sber.spring.FilmLibrary.library_project.model.Director;
import ru.sber.spring.FilmLibrary.library_project.model.Film;
import ru.sber.spring.FilmLibrary.library_project.repository.DirectorRepository;
import ru.sber.spring.FilmLibrary.library_project.repository.FilmRepository;

import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class DirectorService extends GenericService<Director, DirectorDTO> {
    private final DirectorRepository directorRepository;
    private final FilmService filmService;
    protected DirectorService(DirectorRepository directorRepository,
                              DirectorMapper directorMapper,
                              FilmService filmService) {
        super(directorRepository, directorMapper);
        this.directorRepository = directorRepository;
        this.filmService = filmService;
    }



// ДОБАВИТЬ ФИЛЬМ --------------------------------------------------------------------------------------------------------------------------------------------------------------
    public void addFilm(AddFilmDTO addFilmDTO) {
        DirectorDTO director = getOne(addFilmDTO.getDirectorId());
        filmService.getOne(addFilmDTO.getFilmId());
        director.getFilmsIds().add(addFilmDTO.getFilmId());
        update(director);
    }



// ПОИСК УЧАСТНИКОВ -------------------------------------------------------------------------------------------------------------------------------------------------------------
    public Page<DirectorDTO> searchDirectors(final String fio, Pageable pageable) {
        Page<Director> directors = directorRepository.findAllByDirectorFioContainsIgnoreCaseAndIsDeletedFalse(fio, pageable);
        List<DirectorDTO> result = genericMapper.toDTOs(directors.getContent());
        return new PageImpl<>(result, pageable, directors.getTotalElements());
    }



// УДАЛЕНИЕ ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    @Override
    public void deleteSoft(Long objectId) throws MyDeleteException {
        Director director = directorRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Участника с заданным id=" + objectId + " не существует."));
        boolean directorCanBeDeleted = directorRepository.checkDirectorForDeletion(objectId);
        if (directorCanBeDeleted) {
            markAsDeleted(director);
            Set<Film> films = director.getFilms();
            if (films != null && films.size() > 0) {
                films.forEach(this::markAsDeleted);
            }
            directorRepository.save(director);
        }
        else {
            throw new MyDeleteException(Errors.Directors.DIRECTOR_DELETE_ERROR);
        }
    }
    public void restore(Long objectId) {
        Director director = directorRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Автора с заданным id=" + objectId + " не существует."));
        unMarkAsDeleted(director);
        Set<Film> films = director.getFilms();
        if (films != null && films.size() > 0) {
            films.forEach(this::unMarkAsDeleted);
        }
        directorRepository.save(director);
    }
}
