package ru.sber.spring.FilmLibrary.dbexample.model;

import lombok.*;
import java.util.Date;

//POJO - Plain Old Java Object
//@Data // - создает getters & setters & ToString. НО аннотацией пользоваться аккуратною

@Getter // создает стандартные getters
@Setter // создает стандартные setters
@ToString // метод ToString
@NoArgsConstructor // конструктор() по умолчанию
@AllArgsConstructor // конструктор(id, ...)
public class Book {
    @Setter(AccessLevel.NONE) // запретить доступ у сеетеров к этому полю:
    private Integer bookId;
    private String bookTitle;
    private String author;
    private Date dateAdded;
}


















