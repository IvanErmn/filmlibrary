package ru.sber.spring.FilmLibrary.dbexample;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.sber.spring.FilmLibrary.dbexample.constants.DBConstants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration // пометка, чтобы "подключение" поместилось в КОНТЕЙНЕР
@ComponentScan // 

public class MyDBApplicationContext {

    @Bean // пометка, чтобы был доступин везде
    @Scope("singleton") // должен быть ЕДИНСТВЕННЫМ

    public Connection newConnection() throws SQLException { // будет всегда доступен из других классов, т.к. помечен @Bean
        return DriverManager.getConnection("jdbc:postgresql://" + DBConstants.DB_HOST + ":" + DBConstants.PORT + "/" + DBConstants.DB,
                DBConstants.USER,
                DBConstants.PASSWORD);
    }

// Создаем BookDaoBean:
//    @Bean
//    public BookDaoBean bookDaoBean() throws SQLException {
//        return new BookDaoBean((newConnection()));
//    }
}
