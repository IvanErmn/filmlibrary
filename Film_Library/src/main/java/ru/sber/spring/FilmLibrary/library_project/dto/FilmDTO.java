package ru.sber.spring.FilmLibrary.library_project.dto;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.spring.FilmLibrary.library_project.model.Director;
import ru.sber.spring.FilmLibrary.library_project.model.Film;
import ru.sber.spring.FilmLibrary.library_project.model.Genre;
import ru.sber.spring.FilmLibrary.library_project.model.Order;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO extends GenericDTO {
    private String title;
    private Integer premierYear;
    private String country;
    private String filmCompany;
    private String description;
    private String onlineCopyPath;
    private Genre genre;
    private Set<Long> directorsIds;
    private Set<Long> ordersIds;
    private boolean isDeleted;

    public FilmDTO(Film film) {
        FilmDTO filmDTO = new FilmDTO();
        //из entity делаем DTO
        filmDTO.setTitle(film.getTitle());
        filmDTO.setGenre(film.getGenre());
        filmDTO.setPremierYear(film.getPremierYear());
        filmDTO.setId(film.getId());
        filmDTO.setCountry(film.getCountry());
        filmDTO.setDescription(film.getDescription());
        filmDTO.setFilmCompany(film.getFilmCompany());
        Set<Director> directors = film.getDirectors();
        Set<Long> directorsIds = new HashSet<>();
        if (directors != null && directors.size() > 0) {
            directors.forEach(a -> directorsIds.add(a.getId()));
        }
        filmDTO.setDirectorsIds(directorsIds);
    }
}
