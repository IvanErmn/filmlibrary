package ru.sber.spring.FilmLibrary;

import ru.sber.spring.FilmLibrary.library_project.dto.DirectorDTO;
import ru.sber.spring.FilmLibrary.library_project.model.Director;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public interface DirectorTestData {
    DirectorDTO DIRECTOR_DTO_1 = new DirectorDTO("directorFio1",
            "description1",
            "title1",
            new HashSet<>(),
            new HashSet<>(),
            false);

    DirectorDTO DIRECTOR_DTO_2 = new DirectorDTO("directorFio2",
            "description2",
            "title2",
            new HashSet<>(),
            new HashSet<>(),
            false);

    DirectorDTO DIRECTOR_DTO_3_DELETED = new DirectorDTO("directorFio3",
            "description3",
            "title3",
            new HashSet<>(),
            new HashSet<>(),
            true);

    List<DirectorDTO> DIRECTOR_DTO_LIST = Arrays.asList(DIRECTOR_DTO_1, DIRECTOR_DTO_2, DIRECTOR_DTO_3_DELETED);


    Director DIRECTOR_1 = new Director("director1",
            "title1",
            "description1",
            null);

    Director DIRECTOR_2 = new Director("director2",
            "title2",
            "description2",
            null);

    Director DIRECTOR_3 = new Director("director3",
            "title3",
            "description3",
            null);

    List<Director> DIRECTOR_LIST = Arrays.asList(DIRECTOR_1, DIRECTOR_2, DIRECTOR_3);
}
