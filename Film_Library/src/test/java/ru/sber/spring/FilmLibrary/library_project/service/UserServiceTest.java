package ru.sber.spring.FilmLibrary.library_project.service;

import org.junit.jupiter.api.Test;
import ru.sber.spring.FilmLibrary.library_project.dto.UserDTO;
import ru.sber.spring.FilmLibrary.library_project.exception.MyDeleteException;
import ru.sber.spring.FilmLibrary.library_project.model.User;
import ru.sber.spring.FilmLibrary.service.GenericTest;

public class UserServiceTest extends GenericTest<User, UserDTO> {
    @Test
    @Override
    protected void getAll() {
    }

    @Test
    @Override
    protected void getOne() {
    }

    @Test
    @Override
    protected void create() {
    }

    @Test
    @Override
    protected void update() {
    }

    @Test
    @Override
    protected void delete() throws MyDeleteException {
    }

    @Test
    @Override
    protected void restore() {
    }

    @Test
    @Override
    protected void getAllNotDeleted() {
    }
}