package ru.sber.spring.FilmLibrary.service;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.sber.spring.FilmLibrary.library_project.dto.GenericDTO;
import ru.sber.spring.FilmLibrary.library_project.exception.MyDeleteException;
import ru.sber.spring.FilmLibrary.library_project.mapper.GenericMapper;
import ru.sber.spring.FilmLibrary.library_project.model.GenericModel;
import ru.sber.spring.FilmLibrary.library_project.repository.GenericRepository;
import ru.sber.spring.FilmLibrary.library_project.service.GenericService;
import ru.sber.spring.FilmLibrary.library_project.service.userdetails.CustomUserDetails;

public abstract class GenericTest<E extends GenericModel, D extends GenericDTO> {
    protected GenericService<E, D> service;
    protected GenericRepository<E> repository;
    protected GenericMapper<E, D> mapper;

    @BeforeEach
    void init() {
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(CustomUserDetails
                .builder()
                .username("USER"),
                null,
                null);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    protected abstract void getAll();
    protected abstract void getOne();

    protected abstract void create();
    protected abstract void update();

    protected abstract void delete() throws MyDeleteException;
    protected abstract void restore();

    protected abstract void getAllNotDeleted();
}
