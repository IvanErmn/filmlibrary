package ru.sber.spring.FilmLibrary;

import ru.sber.spring.FilmLibrary.library_project.dto.RoleDTO;
import ru.sber.spring.FilmLibrary.library_project.dto.UserDTO;
import ru.sber.spring.FilmLibrary.library_project.model.Role;
import ru.sber.spring.FilmLibrary.library_project.model.User;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

public interface UserTestData {

    UserDTO USER_DTO = new UserDTO(
            "login",
            "password",
            "firstName",
            "lastName",
            "middleName",
            "birthDate",
            "phone",
            "address",
            "email",
            "changePasswordToken",
            false,
            new RoleDTO(),
            new HashSet<>()
    );

    List<UserDTO> USER_DTO_LIST = List.of(USER_DTO);

    User USER = new User(
            "login",
            "password",
            "firstName",
            "lastName",
            "middleName",
            LocalDate.now(),
            "phone",
            "address",
            "email",
            "changePasswordToken",
            new Role(),
            new HashSet<>()
    );

    List<User> USER_LIST = List.of(USER);
}
