package ru.sber.spring.FilmLibrary;

import ru.sber.spring.FilmLibrary.library_project.dto.DirectorDTO;
import ru.sber.spring.FilmLibrary.library_project.dto.FilmDTO;
import ru.sber.spring.FilmLibrary.library_project.dto.FilmWithDirectorsDTO;
import ru.sber.spring.FilmLibrary.library_project.model.Film;
import ru.sber.spring.FilmLibrary.library_project.model.Genre;

import java.io.File;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface FilmTestData {
    FilmDTO FILM_DTO_1 = new FilmDTO(
            "title1",
            2000,
            "country1",
            "filmCompany1",
            "description1",
            "onlineCopyPath1",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>(),
            false);

    FilmDTO FILM_DTO_2 = new FilmDTO(
            "title2",
            2000,
            "country2",
            "filmCompany2",
            "description2",
            "onlineCopyPath2",
            Genre.FANTASY,
            new HashSet<>(),
            new HashSet<>(),
            false);

    List<FilmDTO> FILM_DTO_LIST = Arrays.asList(FILM_DTO_1, FILM_DTO_2);

    Film FILM_1 = new Film(
            "title1",
            2000,
            "country1",
            "filmCompany1",
            "description1",
            "onlineCopyPath1",
            Genre.DRAMA,
            new HashSet<>(),
            new HashSet<>());
    Film FILM_2 = new Film(
            "title2",
            2000,
            "country2",
            "filmCompany2",
            "description2",
            "onlineCopyPath2",
            Genre.FANTASY,
            new HashSet<>(),
            new HashSet<>());

    List<Film> FILM_LIST = Arrays.asList(FILM_1, FILM_2);

    Set<DirectorDTO> DIRECTORS = new HashSet<>(DirectorTestData.DIRECTOR_DTO_LIST);
    FilmWithDirectorsDTO FILM_WITH_DIRECTORS_DTO_1 = new FilmWithDirectorsDTO(FILM_1, DIRECTORS);
    FilmWithDirectorsDTO FILM_WITH_DIRECTORS_DTO_2 = new FilmWithDirectorsDTO(FILM_2, DIRECTORS);

    List<FilmWithDirectorsDTO> FILM_WITH_DIRECTORS_DTO_LIST = Arrays.asList(FILM_WITH_DIRECTORS_DTO_1, FILM_WITH_DIRECTORS_DTO_2);
}
